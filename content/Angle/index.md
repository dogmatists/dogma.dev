# Angle class

## Summary

Language        | Symbol
:---------------| --------------------------------------------------------------
[C][]           | `Angle`
[C++][]         | `dogma::Angle`
[Dart][]        | `Angle`
[Python][]      | `dogma.Angle`
[Ruby][]        | `Dogma::Angle`
[Zig][]         | `dogma.Angle`

[C]:      https://github.com/dogmatists/dogma.c/blob/master/dogma.h
[C++]:    https://github.com/dogmatists/dogma.cpp/blob/master/dogma.hpp
[Dart]:   https://github.com/dogmatists/dogma.dart/blob/master/lib/src/angle.dart
[Python]: https://github.com/dogmatists/dogma.py/blob/master/src/dogma/angle.py
[Ruby]:   https://github.com/dogmatists/dogma.rb/blob/master/lib/dogma/angle.rb
[Zig]:    https://github.com/dogmatists/dogma.zig/blob/master/dogma.zig
