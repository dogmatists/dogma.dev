# Dogma

!!! warning "Here be dragons"

    This is a semi-public, pre-alpha, work-in-progress project.
    
    **Caveat utilitor:**
    Assume nothing works, and you may be pleasantly surprised;
    and when it breaks, you get to keep both pieces.

**This project is presently at an early design and proof-of-concept stage.**
It is unlikely to be particularly useful as yet to anyone but its authors.

If you are interested in the project, you can keep tabs on development [at
GitHub][] and follow the author [on Twitter][] for project announcements.

[at GitHub]:  https://github.com/artob
[on Twitter]: https://twitter.com/bendiken
