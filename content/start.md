# Getting Started

## Summary

Language        | Code
:---------------| --------------------------------------------------------------
[C][]           | `#include <dogma.h>`
[C++][]         | `#include <dogma.hpp>`
[Dart][]        | `import 'package:dogma/dogma.dart';`
[Python][]      | `import dogma`
[Ruby][]        | `require 'dogma'`
[Zig][]         | `const dogma = @import("dogma");`

[C]:      https://github.com/dogmatists/dogma.c
[C++]:    https://github.com/dogmatists/dogma.cpp
[Dart]:   https://github.com/dogmatists/dogma.dart
[Python]: https://github.com/dogmatists/dogma.py
[Ruby]:   https://github.com/dogmatists/dogma.rb
[Zig]:    https://github.com/dogmatists/dogma.zig
