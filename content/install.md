# Installation

## Summary

Language        | Command
:---------------| --------------------------------------------------------------
[C][]           | `git clone https://github.com/dogmatists/dogma.c.git`
[C++][]         | `git clone https://github.com/dogmatists/dogma.cpp.git`
[Dart][]        | `dependencies:↵  dogma: ^0.0.0`
[Python][]      | `pip3 install dogma.py`
[Ruby][]        | `gem install dogma.rb`
[Zig][]         | `git clone https://github.com/dogmatists/dogma.zig.git`

[C]:      https://github.com/dogmatists/dogma.c
[C++]:    https://github.com/dogmatists/dogma.cpp
[Dart]:   https://github.com/dogmatists/dogma.dart
[Python]: https://github.com/dogmatists/dogma.py
[Ruby]:   https://github.com/dogmatists/dogma.rb
[Zig]:    https://github.com/dogmatists/dogma.zig
